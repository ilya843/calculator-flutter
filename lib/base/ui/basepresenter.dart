abstract class BaseView {}

abstract class BasePresenter<T extends BaseView> {
  T view;

  void attachView(T view) {
    this.view = view;
  }
}
