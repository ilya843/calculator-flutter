import 'package:RandomPerson/model/person.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class TableConstants {
  static const PERSON_TABLE = "person";
  static const ID = "id";
  static const LAST_NAME = "lname";
  static const FIRST_NAME = "name";
  static const MIDDLE_NAME = "middleName";
  static const GENDER = "gender";
  static const DATE = "date";
  static const COUNTRY = "country";
  static const POSTCODE = "postcode";
  static const CITY = "city";
  static const STREET = "street";
  static const HOME = "home";
  static const APARTMENT = "apartment";
  static const PHONE = "phone";
  static const COLOR = "color";
  static const LOGIN = "login";
  static const PASSWORD = "password";
  static const USER_PIC = "userpic";
}


Future<Database> getDatabase() async {
  return openDatabase(join(await getDatabasesPath(), "person_db.db"),
      onCreate: (db, version) {
    return db.execute("CREATE TABLE ${TableConstants.PERSON_TABLE} ("
        "${TableConstants.ID} INTEGER PRIMARY KEY,"
        "${TableConstants.LAST_NAME} TEXT,"
        "${TableConstants.FIRST_NAME} TEXT,"
        "${TableConstants.MIDDLE_NAME} TEXT,"
        "${TableConstants.GENDER} TEXT,"
        "${TableConstants.DATE} TEXT,"
        "${TableConstants.COUNTRY} TEXT,"
        "${TableConstants.POSTCODE} TEXT,"
        "${TableConstants.CITY} TEXT,"
        "${TableConstants.STREET} TEXT,"
        "${TableConstants.HOME} INTEGER,"
        "${TableConstants.APARTMENT} INTEGER,"
        "${TableConstants.PHONE} TEXT,"
        "${TableConstants.COLOR} TEXT,"
        "${TableConstants.LOGIN} TEXT,"
        "${TableConstants.PASSWORD} TEXT,"
        "${TableConstants.USER_PIC} TEXT)");
  }, version: 1);
}

Future<void> insertPerson(Person person) async {
  final database = await getDatabase();
  await database.insert(TableConstants.PERSON_TABLE, person.toMap());
}

Future<List<Person>> getPersons() async {
  final db = await getDatabase();
  final List<Map<String, dynamic>> maps = await db.query(TableConstants.PERSON_TABLE);
  return List.generate(maps.length, (i) {
    var map = maps[i];
    return Person(
      name: FullName(
        fname: Name(
          normal: map[TableConstants.FIRST_NAME]
        ),
        lname: Name(
          normal: map[TableConstants.LAST_NAME]
        ),
        patronymic: Name(
          normal: map[TableConstants.MIDDLE_NAME]
        )
      ),
      gender: Gender(
        gender: map[TableConstants.GENDER]
      ),
      date: BirthDate(
        date: map[TableConstants.DATE]
      ),
      address: Address(
        country: map[TableConstants.COUNTRY],
        postcode: map[TableConstants.POSTCODE],
        city: map[TableConstants.CITY],
        street: map[TableConstants.STREET],
        home: map[TableConstants.HOME],
        apartment: map[TableConstants.APARTMENT]
      ),
      phone: map[TableConstants.PHONE],
      login: map[TableConstants.LOGIN],
      password: map[TableConstants.PASSWORD],
      color: map[TableConstants.COLOR],
      picLink: map[TableConstants.USER_PIC]
    );
  });

}


