import 'package:RandomPerson/db/personDatabase.dart';

class Person {
  FullName name;
  Gender gender;
  BirthDate date;
  Address address;
  String phone;
  String login;
  String password;
  String color;
  String picLink;

  Person({this.name,
    this.gender,
    this.date,
    this.address,
    this.phone,
    this.login,
    this.password,
    this.color,
    this.picLink});

  Person.fromJson(Map<String, dynamic> json) {
    name = FullName.fromJson(json['name']);
    gender = Gender.fromJson(json["gender"]);
    date = BirthDate.fromJson(json["date"]);
    address = Address.fromJson(json["address"]);
    phone = json['phone'];
    login = json['login'];
    password = json['password'];
    color = json['color'];
    picLink = json['userpic'];
  }

  Map<String, dynamic> toMap() {
    return {
      TableConstants.LAST_NAME: name.lname.normal,
      TableConstants.FIRST_NAME: name.fname.normal,
      TableConstants.MIDDLE_NAME: name.patronymic.normal,
      TableConstants.GENDER: gender.gender,
      TableConstants.DATE: date.date,
      TableConstants.COUNTRY: address.country,
      TableConstants.POSTCODE: address.postcode,
      TableConstants.CITY: address.city,
      TableConstants.STREET: address.street,
      TableConstants.HOME: address.home,
      TableConstants.APARTMENT: address.apartment,
      TableConstants.PHONE: phone,
      TableConstants.COLOR: color,
      TableConstants.LOGIN: login,
      TableConstants.PASSWORD: password,
      TableConstants.USER_PIC: picLink
    };
  }
}

class FullName {
  Name fname;
  Name lname;
  Name patronymic;

  FullName({this.fname, this.lname, this.patronymic});

  FullName.fromJson(Map<String, dynamic> json) {
    fname = Name.fromJson(json["fname"]);
    lname = Name.fromJson(json["lname"]);
    patronymic = Name.fromJson(json["patronymic"]);
  }
}

class Name {
  String normal;
  String transcription;

  Name({this.normal}) {
    this.transcription = this.normal;
  }

  Name.fromJson(Map<String, dynamic> json) {
    normal = json["normal"];
    transcription = json["transcription"];
  }
}

class Gender {
  String gender;
  int code;

  Gender({this.gender}) {
    this.code = -1;
  }

  Gender.fromJson(Map<String, dynamic> json) {
    gender = json["gender"];
    code = json["code"];
  }
}

class BirthDate {
  String date;
  String digitalTime;

  BirthDate({this.date}){
    this.digitalTime = this.date;
  }

  BirthDate.fromJson(Map<String, dynamic> json) {
    date = json["date"];
    digitalTime = json["digital_time"];
  }
}

class Address {
  String country;
  String postcode;
  String city;
  String street;
  int home;
  int apartment;

  Address({this.country, this.postcode, this.city, this.street, this.home, this.apartment});

  Address.fromJson(Map<String, dynamic> json) {
    country = json["country"];
    postcode = json["postcode"];
    city = json["city"];
    street = json["street"];
    home = json["home"];
    apartment = json["apartment"];
  }
}