import 'package:RandomPerson/loginPage/ui/presenter.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<StatefulWidget> implements LoginPageView {
  bool isLoading = false;
  final LoginPagePresenter presenter = LoginPagePresenter();

  LoginPageState() {
    presenter.attachView(this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Github клиент"),
      ),
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Spacer(),
            textFromField("Логин"),
            textFromField("Пароль"),
            Spacer(),
            buttonWithPadding("Войти", 8),
            Visibility(
              child: CircularProgressIndicator(),
              visible: isLoading,
            )
          ],
        ),
      ),
    );
  }

  Widget textFromField(String labelText) {
    return TextFormField(
      decoration: InputDecoration(labelText: labelText),
    );
  }

  Widget buttonWithPadding(String name, double padding) {
    return Padding(
      padding: EdgeInsets.all(padding),
      child: RaisedButton(
        onPressed: () {
          setState(() {
            isLoading = !isLoading;
          });
        },
        child: Text(name),
      ),
    );
  }
}
