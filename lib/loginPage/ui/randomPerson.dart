import 'dart:convert';

import 'package:RandomPerson/db/personDatabase.dart';
import 'package:RandomPerson/loginPage/ui/personList.dart';
import 'package:RandomPerson/model/person.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class RandomPersonWidget extends StatefulWidget {

  final Person person;

  const RandomPersonWidget({Key key, this.person}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return RandomPersonState(person);
  }
}

class RandomPersonState extends State<StatefulWidget> {
  Person person;
  bool isLoading = true;
  bool isLoadOnStart = true;

  RandomPersonState(this.person) {
    isLoadOnStart = this.person == null;
  }

  @override
  void initState() {
    super.initState();
    if(isLoadOnStart) {
      fetchData();
    } else {
      isLoading = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Случайная личность"),
        ),
        body: Stack(
          children: <Widget>[
            ListView(
              children: <Widget>[
                content(),
                button("Обновить", () {
                  setState(() {
                    person = null;
                    isLoading = true;
                  });
                  fetchData();
                }, bottom: 0),
                button("История личностей", (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => PersonListWidget()));
                })
              ],
            )
          ],
        ));
  }

  Container button(String text, VoidCallback onPressed, {double top = 16.0, double left = 16.0, double bottom = 16.0, double right = 16.0}) {
    return Container(
                margin: EdgeInsets.only(left: left, bottom: bottom, right: right, top: top),
                child: RaisedButton(
                  child: Text(text),
                  onPressed: this.isLoadOnStart ? onPressed : null,
                ),
              );
  }

  Widget content() {
    return isLoading
        ? Center(child: CircularProgressIndicator())
        : personContent(person);
  }

  Widget personContent(Person person) {
    return Padding(
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          imageWidget(person),
          personInfoWithHeader(
              "ФИО", '${person.name.lname.normal} ${person.name.fname.normal} ${person.name.patronymic.normal}'),
          personInfoWithHeader("День рождения", '${person.date.date}'),
          personInfoWithHeader("Город", person.address.city),
          personInfoWithHeader("Улица", person.address.street),
          personInfoWithHeader("Дом", person.address.home.toString()),
          personInfoWithHeader("Квартира", person.address.apartment.toString()),
          personInfoWithHeader("Номер телефона", person.phone),
          personInfoWithHeader("Логин", person.login),
          personInfoWithHeader("Пароль", person.password),
          personInfoWithHeader("Цвет", person.color)
        ],
      ),
    );
  }

  Container personInfoWithHeader(String header, String data) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              header,
              style: TextStyle(fontSize: 14, color: Colors.grey),
            ),
            Container(
              width: double.infinity,
              margin: EdgeInsets.only(top: 8),
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.black),
                  borderRadius: BorderRadius.circular(8)),
              child: Text(
                data,
                style: TextStyle(color: Colors.black, fontSize: 16),
              ),
            )
          ]),
    );
  }

  Center imageWidget(Person person) {
    return Center(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Image.network(
          person.picLink,
          height: 200,
          width: 200,
        ),
      ),
    );
  }

  fetchData() async {
    final response = await get("https://randus.org/api.php");

    if (response.statusCode == 200) {
      var person = Person.fromJson(json.decode(response.body));
      await insertPerson(person);
      setState(() {
        this.person = person;
        isLoading = false;
      });
    } else {
      throw Exception("Failed load post");
    }
  }
}
