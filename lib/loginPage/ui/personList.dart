import 'package:RandomPerson/db/personDatabase.dart';
import 'package:RandomPerson/loginPage/ui/randomPerson.dart';
import 'package:RandomPerson/model/person.dart';
import 'package:flutter/material.dart';

class PersonListWidget extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return PersonListState();
  }

}

class PersonListState extends State<StatefulWidget> {

  List<Person> persons = new List();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("История личностей")
      ),
      body: persons.isNotEmpty ? personList(persons) : Center(child: CircularProgressIndicator()),
    );
  }

  Widget personList(List<Person> persons) {
    return ListView(
      children: persons.map((person) => personWidget(person)).toList()
    );
  }

  Widget personWidget(Person person) {
    return SizedBox(
      width: double.infinity,
      child: Card(
        child: InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => RandomPersonWidget(person: person)));
          },
          child: Row(
            children: <Widget>[
              imageWidget(person.picLink),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(person.name.fname.normal),
                  Text(person.name.lname.normal)
                ],
              )
            ],
          ),
        )
      ),
    );
  }

  Widget imageWidget(String link) {
    return Container(
      margin: EdgeInsets.only(left: 8, top: 8, bottom: 8, right: 8),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Image.network(
          link,
          height: 80,
          width: 80,
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  fetchData() async {
    var personsDb = await getPersons();
    setState(() {
      persons.clear();
      persons.addAll(personsDb);
    });
  }
}